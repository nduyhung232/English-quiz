package com.example.app.english_quiz.model;

import java.io.Serializable;

/**
 * Created by duyhung on 25/01/2018.
 */

public class Option implements Serializable{
    private int id;
    private int idQuestion;
    private String content;
    private boolean correct;

    public Option(int id, int idQuestion, String content, boolean correct) {

        this.id = id;
        this.idQuestion = idQuestion;
        this.content = content;
        this.correct = correct;
    }

    public Option() {
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

}
