package com.example.app.english_quiz.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.model.Level;
import com.example.app.english_quiz.sqlite.Databases;

import java.util.List;

/**
 * Created by duyhung on 25/01/2018.
 */

public class LevelAdapter extends ArrayAdapter {

    Activity context;
    int resource;
    List<Level> objects;
    Databases databases;

    private RatingBar ratingBar;

    public LevelAdapter(@NonNull Activity context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        databases = new Databases(context);
        LayoutInflater inflater = this.context.getLayoutInflater();
        View view = inflater.inflate(this.resource, null);

        Level level = this.objects.get(position);

        TextView textView = view.findViewById(R.id.content);
        TextView number = view.findViewById(R.id.numberLevel);
        ratingBar = view.findViewById(R.id.ratingBar);
        number.setText(String.valueOf(position + 1));
        textView.setText(level.getContent());
        int bestScore = databases.getBestScorebyLevelID(level.getId());
        ratingBar.setRating(bestScore * 5 / databases.getAllQuestionOnOneIdLevel(level.getId()).size());
        Log.d("anhdao: ", String.valueOf(databases.getAllQuestionOnOneIdLevel(level.getId()).size()));

        return view;

    }
}
