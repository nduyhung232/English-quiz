package com.example.app.english_quiz.sqlite.dto;

public class OptionDTO {

    private int id;

    private String content;

    private boolean correct;

    private int idQuestion;

    public OptionDTO(String content, boolean correct, int idQuestion) {
        this.content = content;
        this.correct = correct;
        this.idQuestion = idQuestion;
    }

    public OptionDTO(int id, int idQuestion, String content, boolean correct) {
        this.id = id;
        this.content = content;
        this.correct = correct;
        this.idQuestion = idQuestion;
    }

    public OptionDTO(String content, boolean correct) {
        this.content = content;
        this.correct = correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public String toString() {
        return "OptionDTO{" +
                "content='" + content + '\'' +
                ", correct=" + correct +
                ", idQuestion=" + idQuestion +
                '}';
    }
}
