package com.example.app.english_quiz.sqlite.dto;

import java.util.List;

public class LevelDTO {

    private int id;

    private String name;

    private List<QuestionDTO> questionDTOList;

    private int categoriesId;

    public LevelDTO(String name, int categoriesId) {
        this.name = name;
        this.categoriesId = categoriesId;
    }

    public LevelDTO(int id, int categoriesId, String name) {
        this.id = id;
        this.name = name;
        this.categoriesId = categoriesId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(int categoriesId) {
        this.categoriesId = categoriesId;
    }

    public List<QuestionDTO> getQuestionDTOList() {
        return questionDTOList;
    }

    public void setQuestionDTOList(List<QuestionDTO> questionDTOList) {
        this.questionDTOList = questionDTOList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LevelDTO{" +
                "name='" + name + '\'' +
                ", questionDTOList=" + questionDTOList +
                ", categoriesId=" + categoriesId +
                '}';
    }
}
