package com.example.app.english_quiz.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by duyhung on 25/01/2018.
 */

public class Question implements Serializable {
    private int id;
    private String content;
    private ArrayList<Option> optionsInQuestion;
    private int answer;

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public Question() {
    }

    public Question(int id, String content, int answer){
        this.id = id;
        this.content = content;
        this.answer = answer;
    }

    public ArrayList<Option> getOptionsInQuestion() {
        return optionsInQuestion;
    }

    public void setOptionsInQuestion(ArrayList<Option> optionsInQuestion) {
        this.optionsInQuestion = optionsInQuestion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
