package com.example.app.english_quiz.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.app.english_quiz.BuildConfig;
import com.example.app.english_quiz.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    Button notLogin;
    LoginButton doLogin;
    CallbackManager callbackManager;
    SharedPreferences sharedPreferences;
    public static final String FACEBOOKACCESSTOKEN = "FACEBOOKACCESSTOKEN";
    public static final String ACCESSTOKEN = "ACCESSTOKEN";
    public static final String USERID = "USERID";
    public static final String LOGED = "LOGED";

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        setContentView(R.layout.activity_login);

        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
        if (!loggedIn)
            goToActivity();

        notLogin = (Button) findViewById(R.id.notLogin);
        doLogin = (LoginButton) findViewById(R.id.connectWithFbButton);
        sharedPreferences = getSharedPreferences(FACEBOOKACCESSTOKEN, MODE_PRIVATE);

        if (sharedPreferences.getString(FACEBOOKACCESSTOKEN, null) != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }

        doLogin.setReadPermissions("email");
        doLogin.setReadPermissions("public_profile");

        callbackManager = CallbackManager.Factory.create();
        // If using in a fragmen


        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    private ProfileTracker mProfileTracker;

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                       goToActivity();
                        if (Profile.getCurrentProfile() == null) {

                        } else {
                            Profile profile = Profile.getCurrentProfile();
                            Log.v("facebook - profile", profile.getFirstName());
                        }
                    }

                    @Override
                    public void onCancel() {
                        Log.v("facebook - onCancel", "cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.v("facebook - onError", exception.getMessage());
                    }
                });

        getLogin();
    }

    public void getLogin() {
        notLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(FACEBOOKACCESSTOKEN, LOGED);
                editor.apply();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void goToActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }


}
