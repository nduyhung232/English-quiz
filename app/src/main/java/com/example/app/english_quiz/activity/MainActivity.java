package com.example.app.english_quiz.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.adapter.CategoryAdapter;
import com.example.app.english_quiz.adapter.LevelAdapter;
import com.example.app.english_quiz.model.Categories;
import com.example.app.english_quiz.model.Level;
import com.example.app.english_quiz.sqlite.Databases;
import com.facebook.AccessToken;
import com.facebook.login.LoginResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.app.english_quiz.activity.LoginActivity.FACEBOOKACCESSTOKEN;

public class MainActivity extends AppCompatActivity {

    Databases databases;
    SharedPreferences sharedPreferences;

    ListView lvLevel;
    List<Level> listLevel = new ArrayList<>();
    List<Categories> listCate = new ArrayList<>();
    LevelAdapter levelAdapter;
    CategoryAdapter categoryAdapter;
    ListView listCategories;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databases = new Databases(getApplicationContext());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        addcontrols();
        addevents();
    }

    private void addevents() {
        lvLevel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Level level = (Level) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(MainActivity.this, LevelActivity.class);
                intent.putExtra("level", level);
                startActivity(intent);
            }
        });

        listCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listLevel.clear();
                listLevel.addAll(databases.getAllLevel(listCate.get(position).getId()));
                levelAdapter.notifyDataSetChanged();
                drawer.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void addcontrols() {

        sharedPreferences = getSharedPreferences(FACEBOOKACCESSTOKEN, MODE_PRIVATE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        listCate.addAll(databases.findAllCategory());

        lvLevel = findViewById(R.id.lvLevel);
        listLevel.addAll(databases.getAllLevel(listCate.get(0).getId()));

        levelAdapter = new LevelAdapter(
                MainActivity.this,
                R.layout.item_level_layout,
                listLevel
        );
        lvLevel.setAdapter(levelAdapter);

        listCategories = (ListView) findViewById(R.id.listCategories);
        categoryAdapter = new CategoryAdapter(this, R.layout.item_category_layout, listCate);
        listCategories.setAdapter(categoryAdapter);
    }

    private void handleAccessToken() {
        final String userId = AccessToken.getCurrentAccessToken().getUserId();

        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {

                String filepath = null;
                try {
                    URL url = new URL("https://graph.facebook.com/" + userId + "/picture");
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();
                    File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
                    String filename = "avatar.png";
                    Log.i("Local filename:", "" + filename);
                    File file = new File(SDCardRoot, filename);
                    if (file.createNewFile()) {
                        file.createNewFile();
                    }
                    FileOutputStream fileOutput = new FileOutputStream(file);
                    InputStream inputStream = urlConnection.getInputStream();
                    int totalSize = urlConnection.getContentLength();
                    int downloadedSize = 0;
                    byte[] buffer = new byte[1024];
                    int bufferLength = 0;
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutput.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
                    }
                    fileOutput.close();
                    if (downloadedSize == totalSize) filepath = file.getPath();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    filepath = null;
                    e.printStackTrace();
                }
                Log.i("filepath:", " " + filepath);
                return filepath;
            }
        }.execute();

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

}
