package com.example.app.english_quiz.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.adapter.module_adapter.Result;
import com.example.app.english_quiz.model.Categories;

import java.util.List;

/**
 * Created by developer on 27/02/2018.
 */

public class CategoryAdapter extends ArrayAdapter<Categories> {

    public CategoryAdapter(@NonNull Context context, int resource, @NonNull List<Categories> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_category_layout, null);
            viewHolder.categories = (TextView) convertView.findViewById(R.id.category);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Categories categories = getItem(position);
        viewHolder.categories.setText(categories.getName());
        return convertView;
    }


    class ViewHolder {
        TextView categories;
    }
}
