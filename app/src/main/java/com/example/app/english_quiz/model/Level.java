package com.example.app.english_quiz.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duyhung on 28/01/2018.
 */

public class Level implements Serializable{
    private int id;
    private int idCategory;
    private String content;

    public Level(int id, int idCategory, String content) {
        this.id = id;
        this.idCategory = idCategory;
        this.content = content;
    }   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Level() {

    }
}
