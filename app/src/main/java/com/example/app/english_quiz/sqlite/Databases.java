package com.example.app.english_quiz.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Path;

import com.example.app.english_quiz.model.Categories;
import com.example.app.english_quiz.model.Level;
import com.example.app.english_quiz.model.Option;
import com.example.app.english_quiz.model.Question;
import com.example.app.english_quiz.sqlite.dto.CategoriesDTO;
import com.example.app.english_quiz.sqlite.dto.LevelDTO;
import com.example.app.english_quiz.sqlite.dto.OptionDTO;
import com.example.app.english_quiz.sqlite.dto.QuestionDTO;

import java.util.ArrayList;
import java.util.List;

public class Databases extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "English";

    public static final String TABLE_NAME_CATEGORIES = "categiries";
    public static final String TABLE_NAME_LEVEL = "level";
    public static final String TABLE_NAME_LEVEL_QUESTION = "level_question";
    public static final String TABLE_NAME_QUESTION = "question";
    public static final String TABLE_NAME_OPTION = "`option`";
    public static final String TABLE_SCORE_LEVEL = "level_score";

    public Databases(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // create table
        String create_categories_table = "create table " + TABLE_NAME_CATEGORIES + " (" +
                "id INTEGER PRIMARY KEY NOT NULL, " +
                "name TEXT NOT NULL)";

        String create_level_table = "create table " + TABLE_NAME_LEVEL + " (" +
                "id INTEGER PRIMARY KEY NOT NULL, " +
                "idCategory INTEGER NOT NULL , " +
                "name TEXT NOT NULL," +
                "FOREIGN KEY (idCategory) REFERENCES " + TABLE_NAME_CATEGORIES + "(id))";

        String create_level_question_table = "create table " + TABLE_NAME_LEVEL_QUESTION + " (" +
                "id INTEGER PRIMARY KEY NOT NULL, " +
                "idLevel INTEGER NOT NULL, " +
                "idQuestion INTEGER NOT NULL," +
                "FOREIGN KEY (idLevel) REFERENCES " + TABLE_NAME_LEVEL + "(id)," +
                "FOREIGN KEY (idQuestion) REFERENCES " + TABLE_NAME_QUESTION + "(id))";

        String create_question_table = "create table " + TABLE_NAME_QUESTION + " (" +
                "id INTEGER PRIMARY KEY NOT NULL, " +
                "content TEXT NOT NULL)";

        String create_option_table = "create table " + TABLE_NAME_OPTION + " (" +
                "id INTEGER PRIMARY KEY NOT NULL, " +
                "idQuestion INTEGER NOT NULL, " +
                "content TEXT NOT NULL," +
                "correct BOOLEAN NOT NULL," +
                "FOREIGN KEY (idQuestion) REFERENCES " + TABLE_NAME_QUESTION + "(id))";

        String createScoreTable = "create table " + TABLE_SCORE_LEVEL + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "idLevel INTEGER ," +
                "score INTEGER ," +
                "FOREIGN KEY (idLevel) REFERENCES " + TABLE_NAME_LEVEL + "(id))";

        db.execSQL(create_categories_table);
        db.execSQL(create_level_table);
        db.execSQL(create_question_table);
        db.execSQL(create_level_question_table);
        db.execSQL(create_option_table);
        db.execSQL(createScoreTable);

        for (CategoriesDTO categoriesDTO : allData()) {
            Importer importer = new Importer(db);
            importer.importCategories(categoriesDTO);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public List<Categories> findAllCategory() {
        List<Categories> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select *from " + TABLE_NAME_CATEGORIES;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Categories categories = new Categories(cursor.getInt(0),
                    cursor.getString(1));
            list.add(categories);

        }
        return list;
    }

    public List<Level> getAllLevel(int idCategory) {

        List<Level> listLevel = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select *from " + TABLE_NAME_LEVEL + " where idCategory = " + idCategory;
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Level level = new Level(cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(2));
            listLevel.add(level);
        }
        return listLevel;
    }

    public List<Question> getAllQuestionOnOneIdLevel(int idLevel) {

        List<Question> listQuestion = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select *from " + TABLE_NAME_QUESTION + " where id in (" +
                "select idQuestion from " + TABLE_NAME_LEVEL_QUESTION + " where idLevel = " + idLevel + ")";

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Question question = new Question(cursor.getInt(0),
                    cursor.getString(1),
                    -1);
            listQuestion.add(question);
        }
        return listQuestion;
    }

    public ArrayList<Option> getListOption(int idQuestion) {
        ArrayList<Option> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select *from " + TABLE_NAME_OPTION + " where idQuestion = " + idQuestion;

        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            Option option = new Option(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(2),
                    cursor.getInt(3) > 0 ? true : false);
            list.add(option);
        }

        return list;
    }

    public Option getOption(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select *from " + TABLE_NAME_OPTION + " where id = " + id;

        Cursor cursor = db.rawQuery(sql, null);

        cursor.moveToFirst();
        Option option = new Option(cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getInt(3) > 0 ? true : false);


        return option;
    }

    public void saveBestScore(int score, int levelId) {

        SQLiteDatabase db = this.getReadableDatabase();
        if (checkExistLevelScore(levelId)) {
            ContentValues values = new ContentValues();
            values.put("score", score);
            String where = "idlevel=?";
            String[] whereArgs = new String[] {String.valueOf(levelId)};

            db.update(TABLE_SCORE_LEVEL, values, where, whereArgs);
        } else {
            ContentValues values = new ContentValues();
            values.put("idLevel", levelId);
            values.put("score", score);
            db.insert(TABLE_SCORE_LEVEL, null, values);
        }
    }

    private boolean checkExistLevelScore(int levelID) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from " + TABLE_SCORE_LEVEL + " where idLevel = " + levelID;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst())
            return true;
        return false;
    }

    public int getBestScorebyLevelID(int levelID) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from " + TABLE_SCORE_LEVEL + " where idLevel = " + levelID;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst())
            return cursor.getInt(2);
        return 0;
    }

    private List<CategoriesDTO> allData() {
        List<CategoriesDTO> categoriesDTOList = new ArrayList<>();
        CategoriesDTO categoriesDTO = new CategoriesDTO(1, "Category 1");
        List<LevelDTO> levelDTOList = new ArrayList<>();
        LevelDTO levelDTO = new LevelDTO(1, 1, "Level 1");
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        QuestionDTO questionDTO = new QuestionDTO(1, 1, "I'm sure he _______ being around my ex-husband.");
        List<OptionDTO> optionDTOList = new ArrayList<>();
        OptionDTO optionDTO = new OptionDTO(1, 1, "won't get used", true);
        OptionDTO optionDTO1 = new OptionDTO(2, 1, "is used", false);
        OptionDTO optionDTO2 = new OptionDTO(3, 1, "might be used to", false);
        OptionDTO optionDTO3 = new OptionDTO(4, 1, "will get used to", false);
        optionDTOList.add(optionDTO);
        optionDTOList.add(optionDTO1);
        optionDTOList.add(optionDTO2);
        optionDTOList.add(optionDTO3);
        questionDTO.setOptionDTOList(optionDTOList);
        questionDTOList.add(questionDTO);

        QuestionDTO questionDTO1 = new QuestionDTO(2, 1, "Joe showed me photos of his new car, _____ he bought last week.");
        List<OptionDTO> optionDTOList1 = new ArrayList<>();
        OptionDTO optionDTO4 = new OptionDTO(5, 2, "which", false);
        OptionDTO optionDTO5 = new OptionDTO(6, 2, "that", false);
        OptionDTO optionDTO6 = new OptionDTO(7, 2, "what", true);
        OptionDTO optionDTO7 = new OptionDTO(8, 2, "whom", false);
        optionDTOList1.add(optionDTO4);
        optionDTOList1.add(optionDTO5);
        optionDTOList1.add(optionDTO6);
        optionDTOList1.add(optionDTO7);
        questionDTO1.setOptionDTOList(optionDTOList1);
        questionDTOList.add(questionDTO1);

        QuestionDTO questionDTO2 = new QuestionDTO(3, 1, "When making beer, barley is _____ to bring out its sweetness.");
        List<OptionDTO> optionDTOList2 = new ArrayList<>();
        OptionDTO optionDTO8 = new OptionDTO(9, 3, "germinated", false);
        OptionDTO optionDTO9 = new OptionDTO(10, 3, "malting", false);
        OptionDTO optionDTO10 = new OptionDTO(11, 3, "growing", true);
        OptionDTO optionDTO11 = new OptionDTO(12, 3, "produced", false);
        optionDTOList2.add(optionDTO8);
        optionDTOList2.add(optionDTO9);
        optionDTOList2.add(optionDTO10);
        optionDTOList2.add(optionDTO11);
        questionDTO2.setOptionDTOList(optionDTOList2);

        questionDTOList.add(questionDTO2);
        ;

        levelDTO.setQuestionDTOList(questionDTOList);

        LevelDTO levelDTO1 = new LevelDTO(2, 1, "Level 2");
        List<QuestionDTO> questionDTOList3 = new ArrayList<>();
        QuestionDTO questionDTO3 = new QuestionDTO(4, 2, "The guest room is quite small, but there should be enough space for you there. You only have a few ____ and pieces.");
        List<OptionDTO> optionDTOList3 = new ArrayList<>();
        OptionDTO optionDTO12 = new OptionDTO(13, 4, "bits", true);
        OptionDTO optionDTO13 = new OptionDTO(14, 4, "Iclothes", false);
        OptionDTO optionDTO14 = new OptionDTO(15, 4, "stuff", false);
        OptionDTO optionDTO15 = new OptionDTO(16, 4, "books", false);
        optionDTOList3.add(optionDTO12);
        optionDTOList3.add(optionDTO13);
        optionDTOList3.add(optionDTO14);
        optionDTOList3.add(optionDTO15);
        questionDTO3.setOptionDTOList(optionDTOList3);
        questionDTOList3.add(questionDTO3);

        QuestionDTO questionDTO4 = new QuestionDTO(5, 2, "Seldom _____ anything funnier.");
        List<OptionDTO> optionDTOList4 = new ArrayList<>();
        OptionDTO optionDTO16 = new OptionDTO(17, 5, "I have seen", false);
        OptionDTO optionDTO17 = new OptionDTO(18, 5, "I see", false);
        OptionDTO optionDTO18 = new OptionDTO(19, 5, "have I seen", true);
        OptionDTO optionDTO19 = new OptionDTO(20, 5, "I saw", false);
        optionDTOList4.add(optionDTO16);
        optionDTOList4.add(optionDTO17);
        optionDTOList4.add(optionDTO18);
        optionDTOList4.add(optionDTO19);
        questionDTO4.setOptionDTOList(optionDTOList4);
        questionDTOList3.add(questionDTO4);

        QuestionDTO questionDTO5 = new QuestionDTO(6, 2, "_____ not been for his help, I couldn't have passed.");
        List<OptionDTO> optionDTOList5 = new ArrayList<>();
        OptionDTO optionDTO20 = new OptionDTO(21, 6, "If it", false);
        OptionDTO optionDTO21 = new OptionDTO(22, 6, "Was he", false);
        OptionDTO optionDTO22 = new OptionDTO(23, 6, "Had it", true);
        OptionDTO optionDTO23 = new OptionDTO(24, 6, "If he had", false);
        optionDTOList5.add(optionDTO20);
        optionDTOList5.add(optionDTO21);
        optionDTOList5.add(optionDTO22);
        optionDTOList5.add(optionDTO23);
        questionDTO5.setOptionDTOList(optionDTOList5);

        questionDTOList3.add(questionDTO5);
        ;

        levelDTO.setQuestionDTOList(questionDTOList3);

        LevelDTO levelDTO2 = new LevelDTO(3, 1, "Level 3");
        List<QuestionDTO> questionDTOList2 = new ArrayList<>();
        QuestionDTO questionDTO6 = new QuestionDTO(7, 3, "The judge had _____ in allowing the lawsuit to proceed.");
        List<OptionDTO> optionDTOList6 = new ArrayList<>();
        OptionDTO optionDTO24 = new OptionDTO(25, 7, "astrayed", true);
        OptionDTO optionDTO25 = new OptionDTO(26, 7, "erred", false);
        OptionDTO optionDTO26 = new OptionDTO(27, 7, "biased", false);
        OptionDTO optionDTO27 = new OptionDTO(28, 7, "drifted", false);
        optionDTOList6.add(optionDTO24);
        optionDTOList6.add(optionDTO25);
        optionDTOList6.add(optionDTO26);
        optionDTOList6.add(optionDTO27);
        questionDTO6.setOptionDTOList(optionDTOList6);
        questionDTOList2.add(questionDTO6);

        QuestionDTO questionDTO7 = new QuestionDTO(8, 3, "How are you ?");
        List<OptionDTO> optionDTOList7 = new ArrayList<>();
        OptionDTO optionDTO28 = new OptionDTO(29, 8, "I am here", false);
        OptionDTO optionDTO29 = new OptionDTO(30, 8, "I am 18 year old", false);
        OptionDTO optionDTO30 = new OptionDTO(31, 8, "I am sick", true);
        OptionDTO optionDTO31 = new OptionDTO(32, 8, "I am Tien Dung", false);
        optionDTOList7.add(optionDTO28);
        optionDTOList7.add(optionDTO29);
        optionDTOList7.add(optionDTO30);
        optionDTOList7.add(optionDTO31);
        questionDTO7.setOptionDTOList(optionDTOList7);
        questionDTOList2.add(questionDTO7);

        QuestionDTO questionDTO8 = new QuestionDTO(9, 3, "How are you ?");
        List<OptionDTO> optionDTOList8 = new ArrayList<>();
        OptionDTO optionDTO32 = new OptionDTO(33, 9, "I am here", false);
        OptionDTO optionDTO33 = new OptionDTO(34, 9, "I am 18 year old", false);
        OptionDTO optionDTO34 = new OptionDTO(35, 9, "I am sick", true);
        OptionDTO optionDTO35 = new OptionDTO(36, 9, "I am Tien Dung", false);
        optionDTOList8.add(optionDTO32);
        optionDTOList8.add(optionDTO33);
        optionDTOList8.add(optionDTO34);
        optionDTOList8.add(optionDTO35);
        questionDTO8.setOptionDTOList(optionDTOList8);

        questionDTOList2.add(questionDTO8);
        ;

        levelDTO1.setQuestionDTOList(questionDTOList3);
        levelDTO2.setQuestionDTOList(questionDTOList2);

        levelDTO.setQuestionDTOList(questionDTOList);

        levelDTOList.add(levelDTO);
        levelDTOList.add(levelDTO1);
        levelDTOList.add(levelDTO2);
        categoriesDTO.setLevelDTOS(levelDTOList);
        categoriesDTOList.add(categoriesDTO);
        return categoriesDTOList;
    }
}
