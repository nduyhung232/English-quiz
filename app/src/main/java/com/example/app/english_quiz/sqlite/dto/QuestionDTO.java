package com.example.app.english_quiz.sqlite.dto;

import java.util.List;

public class QuestionDTO {

    private int id;

    private String content;

    private int levelId;

    private List<OptionDTO> optionDTOList;

    public QuestionDTO(String content, int levelId) {
        this.content = content;
        this.levelId = levelId;
    }

    public QuestionDTO(int id, int levelId, String content) {
        this.id = id;
        this.content = content;
        this.levelId = levelId;
    }

    public QuestionDTO(String content, int levelId, List<OptionDTO> optionDTOList) {
        this.content = content;
        this.levelId = levelId;
        this.optionDTOList = optionDTOList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public List<OptionDTO> getOptionDTOList() {
        return optionDTOList;
    }

    public void setOptionDTOList(List<OptionDTO> optionDTOList) {
        this.optionDTOList = optionDTOList;
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
                "content='" + content + '\'' +
                ", levelId=" + levelId +
                ", optionDTOList=" + optionDTOList +
                '}';
    }
}
