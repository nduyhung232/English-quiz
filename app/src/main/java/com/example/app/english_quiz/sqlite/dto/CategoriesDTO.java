package com.example.app.english_quiz.sqlite.dto;

import java.util.List;

public class CategoriesDTO {

    private int id;

    private String name;

    List<LevelDTO> levelDTOS;

    public CategoriesDTO() {
    }

    public CategoriesDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoriesDTO(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LevelDTO> getLevelDTOS() {
        return levelDTOS;
    }

    public void setLevelDTOS(List<LevelDTO> levelDTOS) {
        this.levelDTOS = levelDTOS;
    }

    @Override
    public String toString() {
        return "CategoriesDTO{" +
                "name='" + name + '\'' +
                ", levelDTOS=" + levelDTOS +
                '}';
    }
}
