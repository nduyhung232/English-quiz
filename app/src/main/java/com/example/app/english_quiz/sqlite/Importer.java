package com.example.app.english_quiz.sqlite;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.example.app.english_quiz.sqlite.dto.CategoriesDTO;
import com.example.app.english_quiz.sqlite.dto.LevelDTO;
import com.example.app.english_quiz.sqlite.dto.OptionDTO;
import com.example.app.english_quiz.sqlite.dto.QuestionDTO;

import static com.example.app.english_quiz.sqlite.Databases.TABLE_NAME_CATEGORIES;
import static com.example.app.english_quiz.sqlite.Databases.TABLE_NAME_LEVEL;
import static com.example.app.english_quiz.sqlite.Databases.TABLE_NAME_LEVEL_QUESTION;
import static com.example.app.english_quiz.sqlite.Databases.TABLE_NAME_OPTION;
import static com.example.app.english_quiz.sqlite.Databases.TABLE_NAME_QUESTION;

/**
 * Created by thetai on 24/02/18.
 */

public class Importer {

    private CategoriesDTO categoriesDTO;

    private SQLiteDatabase db;

    public Importer( SQLiteDatabase db) {
        this.db = db;
    }

    public void importCategories(CategoriesDTO categoriesDTO) {

        ContentValues values = new ContentValues();
        values.put("id", categoriesDTO.getId());
        values.put("name", categoriesDTO.getName());
        db.insert(TABLE_NAME_CATEGORIES, null, values);
        for (LevelDTO levelDTO : categoriesDTO.getLevelDTOS()) {
            importLevel(levelDTO);
        }
    }

    private void importLevel(LevelDTO levelDTO) {
        ContentValues values = new ContentValues();
        values.put("id", levelDTO.getId());
        values.put("name", levelDTO.getName());
        values.put("idCategory", levelDTO.getCategoriesId());
        db.insert(TABLE_NAME_LEVEL, null, values);
        for (QuestionDTO questionDTO : levelDTO.getQuestionDTOList()) {
            importQuestion(questionDTO);
        }
    }

    private void importLevelQuestion(int levelID, int questionID) {
        ContentValues values = new ContentValues();
        values.put("idLevel", levelID);
        values.put("idQuestion", questionID);
        db.insert(TABLE_NAME_LEVEL_QUESTION, null, values);
    }

    private void importQuestion(QuestionDTO questionDTO) {
        ContentValues values = new ContentValues();
        values.put("id", questionDTO.getId());
        values.put("content", questionDTO.getContent());
        db.insert(TABLE_NAME_QUESTION, null, values);
        importLevelQuestion(questionDTO.getLevelId(), questionDTO.getId());
        for (OptionDTO optionDTO : questionDTO.getOptionDTOList()) {
            importOption(optionDTO);
        }
    }

    private void importOption(OptionDTO optionDTO) {
        ContentValues values = new ContentValues();
        values.put("id", optionDTO.getId());
        values.put("content", optionDTO.getContent());
        values.put("correct", optionDTO.isCorrect());
        values.put("idQuestion", optionDTO.getIdQuestion());
        db.insert(TABLE_NAME_OPTION, null, values);
    }
}
