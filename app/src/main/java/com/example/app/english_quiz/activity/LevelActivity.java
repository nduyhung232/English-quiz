package com.example.app.english_quiz.activity;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.adapter.module_adapter.Result;
import com.example.app.english_quiz.model.Level;
import com.example.app.english_quiz.model.Option;
import com.example.app.english_quiz.model.Question;
import com.example.app.english_quiz.sqlite.Databases;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LevelActivity extends AppCompatActivity {

    Databases databases;
    Level level;
    List<Question> listQuestion = new ArrayList<>();
    Result[] resultList;
    TextView textQuestion, textPage;
    RadioGroup radioGroup;
    Button btnPrevious, btnNext;
    TextView txtlevel;

    // the first page = 0;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        databases = new Databases(getApplicationContext());

        addControls();
        addEvents();

    }

    private void addControls() {
        Intent intent = getIntent();
        level = (Level) intent.getSerializableExtra("level");

        textQuestion = findViewById(R.id.txtQuestion);
        textPage = findViewById(R.id.txtPage);
        radioGroup = findViewById(R.id.groupChoice);
        btnNext = findViewById(R.id.btnNext);
        btnPrevious = findViewById(R.id.btnPrevious);
        txtlevel = findViewById(R.id.nameLevel);
        txtlevel.setText(level.getContent());

        listQuestion.clear();
        listQuestion.addAll(databases.getAllQuestionOnOneIdLevel(level.getId()));
        resultList = new Result[listQuestion.size()];

        for (int i = 0; i < listQuestion.size(); i++) {
            ArrayList<Option> list = new ArrayList<>();
            list.addAll(databases.getListOption(listQuestion.get(i).getId()));
            listQuestion.get(i).setOptionsInQuestion(list);
        }

        reFresh();
    }

    private void addEvents() {

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position != 0) {
                    position--;
                    reFresh();
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position != listQuestion.size() - 1) {
                    position++;
                    reFresh();
                } else {
                    Intent intent = new Intent(LevelActivity.this, ResultActivity.class);
                    intent.putExtra("listResult", (Serializable) Arrays.asList(resultList));
                    intent.putExtra("levelID",level.getId());
                    startActivity(intent);
                }
            }
        });
    }

    private void reFresh() {
        setText();

        radioGroup.removeAllViews();
        final List<RadioButton> radioButtons = new ArrayList<>();
        final Question question = listQuestion.get(position);
        final Result result = new Result();
        result.setQuestion(question.getContent());
        for (int i = 0; i < question.getOptionsInQuestion().size(); i++) {
            final RadioButton rb = new RadioButton(this);
            final Option option = listQuestion.get(position).getOptionsInQuestion().get(i);
            rb.setText(option.getContent());
            rb.setId(option.getId());

            if (resultList[position] != null)
                if (resultList[position].getIdAnswer() == option.getId())
                    rb.setChecked(true);

            if (option.isCorrect())
                result.setCorrectAnswer(option.getContent());

            rb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (RadioButton radioButton : radioButtons) {
                        if (radioButton.getId() != rb.getId())
                            radioButton.setChecked(false);
                    }
                    result.setYourAnswer(rb.getText().toString());
                    result.setIdAnswer(rb.getId());
                    if (option.isCorrect())
                        result.setIscorrect(true);
                    resultList[position] = result;
                }
            });
            radioGroup.addView(rb);
            radioButtons.add(rb);
        }
    }

    private void setText() {
        textQuestion.setText(listQuestion.get(position).getContent());

        textPage.setText(position + 1 + "/" + listQuestion.size());

        if (position != 0)
            btnPrevious.setText("Previous");
        else
            btnPrevious.setText("");
        if (position != listQuestion.size() - 1)
            btnNext.setText("Next");
        else
            btnNext.setText("Finish");

    }

}
