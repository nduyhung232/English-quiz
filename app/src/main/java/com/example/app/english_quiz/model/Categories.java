package com.example.app.english_quiz.model;

import java.io.Serializable;

/**
 * Created by duyhung on 25/01/2018.
 */

public class Categories implements Serializable{
    private int id;
    private String name;

    public Categories() {
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Categories(int id, String name) {

        this.id = id;
        this.name = name;
    }
}
