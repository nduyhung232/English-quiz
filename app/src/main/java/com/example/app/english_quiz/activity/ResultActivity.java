package com.example.app.english_quiz.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.adapter.ResultAdapter;
import com.example.app.english_quiz.adapter.module_adapter.Result;
import com.example.app.english_quiz.sqlite.Databases;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ResultActivity extends AppCompatActivity {

    ResultAdapter resultAdapter;
    List<Result> resultList;

    ListView listView;

    TextView textView;
    int levelID;
    Databases databases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        databases = new Databases(getApplicationContext());

        listView = (ListView) findViewById(R.id.listResult);
        textView = (TextView) findViewById(R.id.statistical);
        resultList = new ArrayList<>();
        resultList.addAll((Collection<? extends Result>) getIntent().getExtras().get("listResult"));
        levelID = getIntent().getIntExtra("levelID", 0);
        resultAdapter = new ResultAdapter(getApplicationContext(), R.layout.item_result_layout, resultList);
        listView.setAdapter(resultAdapter);
        statisticalLevel();
    }

    private void statisticalLevel() {
        int counter = 0;
        for (Result result : resultList) {
            if (result.isIscorrect())
                counter++;
        }
        textView.setText(counter + "/(" + resultList.size() + ")");
        if (counter > databases.getBestScorebyLevelID(levelID))
            databases.saveBestScore(counter, levelID);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ResultActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
