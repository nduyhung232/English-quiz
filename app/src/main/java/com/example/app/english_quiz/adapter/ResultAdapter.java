package com.example.app.english_quiz.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.app.english_quiz.R;
import com.example.app.english_quiz.adapter.module_adapter.Result;

import java.util.List;

/**
 * Created by developer on 27/02/2018.
 */

public class ResultAdapter extends ArrayAdapter<Result> {

    public ResultAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_result_layout, null);
            viewHolder.question = (TextView) convertView.findViewById(R.id.question);
            viewHolder.yourAnswer = (TextView) convertView.findViewById(R.id.yourAnswer);
            viewHolder.correctAnswer = (TextView) convertView.findViewById(R.id.correctAnswer);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Result result = getItem(position);
        viewHolder.question.setText(result.getQuestion());
        if (result.getYourAnswer() == null)
            viewHolder.yourAnswer.setText("your answer:");
        else
            viewHolder.yourAnswer.setText("your answer: " + result.getYourAnswer());

        viewHolder.correctAnswer.setText("correct answer: " + result.getCorrectAnswer());

        if (result.isIscorrect())
            viewHolder.yourAnswer.setTextColor(Color.GREEN);
        return convertView;
    }


    class ViewHolder {
        TextView question;
        TextView yourAnswer;
        TextView correctAnswer;
    }
}
